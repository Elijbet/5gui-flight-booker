import React, { useState, useEffect } from 'react';
import './App.css';
import Dropdown from './Dropdown'

function App() {
  const values = ['One-Way', 'Return']

  const [selected, setSelected] = useState(0)
  const [dates, setDates] = useState({forward: '05/14/2021', back: '05/14/2021'})
  const [validDates, setValidDates] = useState({forwardValid: true, backValid: true})

  useEffect(() => {
    if(selected === 0){
      setDates({...dates, back: ''})
    }else if(selected === 1){
      setDates({...dates, back: '05/14/2021'})
    }
  }, [selected]);

  useEffect(() => {
    if(dates.forward.length > 10 || validate(dates.forward) === false && dates.forward !== ''){
      setValidDates({...validDates, forwardValid: false})
    }else{
      setValidDates({...validDates, forwardValid: true})
    }
  }, [dates.forward])

  useEffect(() => {
    if(dates.back.length > 10 || validate(dates.back) === false && dates.back !== ''){
      setValidDates({...validDates, backValid: false})
    }else{
      setValidDates({...validDates, backValid: true})
    }
  }, [dates.back])

  const toggleItem = (id) => {
    setSelected(id)
  }

  const validate = (input) => {
    const toLocaleDateStringFormat = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    return toLocaleDateStringFormat.test(input)
  }

  const handleForward = (event) => {
    setDates({...dates, forward: event.target.value})
  }

  const handleBack = (event) => {
    setDates({...dates, back: event.target.value})
  }
  const handleBooking = () => {
    let message = "";
    if(selected === 0){
      message = `You booked a ${values[selected]} flight on ${dates.forward}`
    }else if(selected === 1){
      message = `You booked a round trip flight from ${dates.forward} to ${dates.back}`
    }
    alert(message)
  }

  const getStatus = () => {
    //When it's one-way flight and field is valid button is enabled. 
    if(selected === 0 && validDates.forwardValid){
      return false
    //When it's a two way flight and both fields are valid and second date is on/after the first date button is enabled 
    }else if(selected === 1 && validDates.forwardValid && validDates.backValid && dates.back >= dates.forward){
      return false
    }else{
      return true
    }
  }

  const buttonClass = () => {
    if(selected === 1 && dates.back < dates.forward){
      return 'disabled'
    }else if (selected === 0){
      return ''
    }
  }

  return (
    <div className="Container">
      <Dropdown  
        list = {values}
        toggleItem = {toggleItem}
        selected = {selected}
      />
      <input  
        value={dates.forward} 
        className={[validDates.forwardValid ? '' : 'invalid'].join(' ')} 
        onChange={handleForward}
      />
      <input  
        value={dates.back} 
        className={[selected === 0 ? 'disabled' : '', validDates.backValid ? '' : 'invalid'].join(' ')} 
        disabled={selected === 0 ? true : false} 
        onChange={handleBack}
      />
      <button 
        className={['final',  buttonClass()].join(' ')} 
        disabled={getStatus()} 
        onClick={handleBooking}
      >
        Book
      </button>
    </div>
  );
}

export default App;

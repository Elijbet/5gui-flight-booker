import React, { useState } from 'react';
import './App.css';
import 'font-awesome/css/font-awesome.min.css';

const Dropdown = ({list, toggleItem, selected}) => {
  
  const [isListOpen, setIsListOpen] = useState(false);
  const toggleList = () => setIsListOpen(!isListOpen);
  
  const check = (key) => {
    return selected === key ? <i class="fa fa-check-square-o"></i> : <i class="fa fa-square-o"></i>
  }
  const highlight = (key) => {
    return selected === key ? 'selected' : ''
  }
    
  return (
    <>
      <button
        type="button"
        onClick={toggleList}
        className={isListOpen ? 'half-height' : 'full-height'}
      >
        {isListOpen
          ? <i className="fa fa-angle-up fa-2x"></i>
          : <i className="fa fa-angle-down fa-2x"></i>}
      </button>
      {isListOpen && (
        <div
          className="dd-list"
        >
          {list.map((val, index) => (
            <button
              className = {[highlight(index), "align"].join(' ')}
              key={index}
              onClick={() => toggleItem(index)}
            >
              {val}
              {check(index)}
            </button>
          ))}
        </div>
      )}
    </>
 )
};

export default Dropdown;